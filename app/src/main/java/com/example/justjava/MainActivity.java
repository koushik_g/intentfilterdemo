package com.example.justjava;



import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.text.NumberFormat;

/**
 * This app displays an order form to order coffee.
 */
public class MainActivity extends AppCompatActivity {

    private TextView quantityTextView, orderSummaryTextView;
    private EditText nameTxt;
    private CheckBox whippedCheckBox, chocolateCheckBox;
    int quantity = 0 ;
    String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeUI();
    }

    private void initializeUI() {
        whippedCheckBox = findViewById(R.id.check_box);
        chocolateCheckBox = findViewById(R.id.chocolate_check_box);
        nameTxt = findViewById(R.id.name_edit_txt);
        quantityTextView = (TextView) findViewById(R.id.quantity_text_view);
    }

    /**
     * This method is called when the order button is clicked.
     */
    public void submitOrder(View view) {
        int price = calculatePrice();

        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.putExtra(Intent.EXTRA_TEXT, createOrderSummary(price));
        intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.email_subject, name));
        intent.setData(Uri.parse(getString(R.string.intent_data_type)));
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    private int calculatePrice(){
        int basePrice = 5;
        if (whippedCheckBox.isChecked()) {
            basePrice += 1;
        }
        if (chocolateCheckBox.isChecked()) {
            basePrice += 2;
        }
        return basePrice * quantity;
    }

    /**
     * This method displays the given quantity value on the screen.
     */
    private void display(int number) {
        quantityTextView.setText("" + number);
    }

    public void increment(View view){
        if (quantity == 100) {
            Toast.makeText(this, getString(R.string.max_limit_error_message), Toast.LENGTH_SHORT).show();
            return;
        }
        quantity = quantity + 1;
        display(quantity);
    }

    public void decrement(View view){
        if (quantity == 0) {
            Toast.makeText(this, getString(R.string.min_limit_error_message), Toast.LENGTH_SHORT).show();
            return;
        }
        quantity = quantity - 1;
        display(quantity);
    }

    private String createOrderSummary(int price) {
        name = nameTxt.getText().toString();
        int length = name.length();
        boolean isWhippedCheckBoxChecked = whippedCheckBox.isChecked();
        boolean isChocolateBoxChecked = chocolateCheckBox.isChecked();
        String orderSummary = getString(R.string.order_summary_name, name);
        orderSummary += "\n" + getString(R.string.order_summary_whipped_cream, isWhippedCheckBoxChecked);
        orderSummary += "\n" + getString(R.string.order_summary_chocolate, isChocolateBoxChecked);
        orderSummary += "\n" + getString(R.string.order_summary_quantity, quantity);
        orderSummary += "\n" + getString(R.string.order_summary_total, NumberFormat.getCurrencyInstance().format(price))+ "\n" +getString(R.string.order_summary_thank_you);

        return orderSummary;
    }
}