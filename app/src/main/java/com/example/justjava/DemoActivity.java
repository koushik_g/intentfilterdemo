package com.example.justjava;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;


public class DemoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo);
    }

    public void buttonClicked(View v) {
        ImageView image = findViewById(R.id.image_view);
        image.setImageResource(R.drawable.after_cookie);
        TextView textView = findViewById(R.id.text_view);
        textView.setText("I'm so Full");
    }
}
